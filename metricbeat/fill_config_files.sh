#!/usr/bin/env bash

for module in nginx postgresql rabbitmq redis
do
    if [[ $ENABLED_MODULES = *"$module"* ]]
    then
        dockerize -template /tmp/metricbeat/$module.tmpl:/usr/share/metricbeat/modules.d/$module.yml
    else
        echo "$module not used"
    fi
done