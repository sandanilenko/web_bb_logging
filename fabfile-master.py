from fabric import api as fab
from fabricio import tasks, docker

fab.env.roledefs.update(
    web=[],
)

fab.env.update(
    master_env=[],
)


@tasks.infrastructure
def test():
    fab.env.master_env.extend([
        "bootstrap.memory_lock=true",
        'ES_JAVA_OPTS=-Xms2048m -Xmx2048m'
    ])

    fab.env.update(
        roledefs={
            'web': ['sandan@192.168.2.39'],
        },
    )


@tasks.infrastructure
def stage():
    fab.env.master_env.extend([
        "bootstrap.memory_lock=true",
        'ES_JAVA_OPTS=-Xms2048m -Xmx2048m'
    ])

    fab.env.update(
        roledefs={
            'web': ['root@192.168.228.246'],
        },
    )


elasticsearch = tasks.ImageBuildDockerTasks(
    build_path='./elasticsearch',
    service=docker.Container(
        name='elasticsearch',
        image='192.168.228.246:5000/logging/elasticsearch:latest',
        options=dict(
            publish=['9200:9200', '9300:9300'],
            network='elk',
            volume=['esdata:/usr/share/elasticsearch/data', ],
            env=fab.env.master_env,
            ulimit=['memlock=-1:-1', 'nofile=65536:65536']
        ),

    ),
    roles=['web']
)


kibana = tasks.DockerTasks(
    service=docker.Container(
        name='kibana',
        image='docker.elastic.co/kibana/kibana-oss:6.6.0',
        options=dict(
            publish='5601:5601',
            network='elk'
        )
    ),
    roles=['web']
)