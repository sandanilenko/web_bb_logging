from fabric import api as fab
from fabricio import tasks, docker

fab.env.roledefs.update(
    web=[],
)

fab.env.update(
    metricbeat_env=[],
)


@tasks.infrastructure
def server_192_168_2_27():
    fab.env.update(
        roledefs={
            'web': ['sandan@192.168.2.27'],
        },
    )

    fab.env.metricbeat_env.extend([
        'ELASTICSEARCH_HOSTS="192.168.2.39:9200"',
        'KIBANA_HOST="192.168.2.39:5601"',
        'NGINX_PORT=80',
        'DATABASE_PORT=5432',
        'DATABASE_USERNAME=postgres',
        'DATABASE_PASSWORD=postgres',
        'RABBITMQ_PORT=5672',
        'RABBITMQ_USERNAME=web_bb',
        'RABBITMQ_PASSWORD=web_bb',
        'REDIS_PORT=6379',
        'ENABLED_MODULES=system postgresql rabbitmq redis nginx',
        'METRICBEAT_NAME=192_168_2_27'
    ])


@tasks.infrastructure
def server_192_168_228_97():
    fab.env.update(
        roledefs={
            'web': ['a.danilenko@192.168.228.97']
        }
    )

    fab.env.metricbeat_env.extend([
        'ELASTICSEARCH_HOSTS="192.168.228.246:9200"',
        'KIBANA_HOST="192.168.228.246:5601"',
        'NGINX_PORT=80',
        'DATABASE_PORT=5432',
        'DATABASE_USERNAME=postgres',
        'DATABASE_PASSWORD=postgres',
        'RABBITMQ_PORT=5672',
        'RABBITMQ_USERNAME=web_bb',
        'RABBITMQ_PASSWORD=web_bb',
        'REDIS_PORT=6379',
        'ENABLED_MODULES=system postgresql rabbitmq redis nginx',
        'METRICBEAT_NAME=192_168_228_97'
    ])

metricbeat = tasks.ImageBuildDockerTasks(
    build_path='./metricbeat',
    service=docker.Container(
        name='metricbeat',
        image='192.168.228.246:5000/logging/metricbeat:latest',
        options=dict(
            restart='always',
            volume=[
                '/proc:/hostfs/proc:ro',
                '/sys/fs/cgroup:/hostfs/sys/fs/cgroup:ro',
                '/:/hostfs:ro'
            ],
            env=fab.env.metricbeat_env,
            network='host'
        )
    ),
    roles=['web']
)
